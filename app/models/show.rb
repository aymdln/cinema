class Show < ApplicationRecord
  belongs_to :movie
  enum language: ["VF", "VOSTFR"]
end
