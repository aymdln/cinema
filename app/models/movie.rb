class Movie < ApplicationRecord
  has_many :shows
  has_many :movie_credits
  has_many :movie_genres

  def poster_500
    "https://image.tmdb.org/t/p/w500#{self.poster_path}"
  end
  
end
