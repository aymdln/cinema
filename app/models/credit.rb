class Credit < ApplicationRecord
  has_many :movies
  has_many :movie_credits
end
