class MovieCredit < ApplicationRecord
  belongs_to :credit
  belongs_to :movie
end
