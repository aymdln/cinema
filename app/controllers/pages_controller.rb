class PagesController < ApplicationController
  def home
    now = Time.now
    @shows = Show.where("date >= ?", now)
    @backdrops = @shows.map do |show|
      show.movie.backdrop_path
    end
    @movies = @shows.map do |show|
      show.movie
    end
    @movies = @movies.uniq
    # raise
  end
  
end