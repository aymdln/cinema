require 'net/http'
require 'net/https'
require 'uri'

uri = URI('https://EMS0226:b4c2f3777b9f894a08bcd5178fe5b67b@movies.monnaie-services.com/FR/prog/v0/?version=0')

Net::HTTP.start(uri.host, uri.port,
  :use_ssl => uri.scheme == 'https', 
  :verify_mode => OpenSSL::SSL::VERIFY_NONE) do |http|

  request = Net::HTTP::Get.new uri.request_uri
  request.basic_auth 'EMS0226', 'b4c2f3777b9f894a08bcd5178fe5b67b'

  response = http.request request # Net::HTTPResponse object

  puts response
  puts response.body
end