# puts "Destroy Table ..."
# Movie.destroy_all
# Credit.destroy_all
# MovieCredit.destroy_all

uri = URI("https://#{ENV["USER_CAISSE"]}:#{ENV["PASSWORD_CAISSE"]}@movies.monnaie-services.com/FR/prog/v0/?version=0")

Net::HTTP.start(uri.host, uri.port,
                :use_ssl => uri.scheme == "https",
                :verify_mode => OpenSSL::SSL::VERIFY_NONE) do |http|
  request = Net::HTTP::Get.new uri.request_uri
  request.basic_auth ENV["USER_CAISSE"], ENV["PASSWORD_CAISSE"]

  response = http.request request # Net::HTTPResponse object

  caisse = JSON.parse(response.body)
  events = caisse["sites"].first["events"]

  events.each do |event|
    title = I18n.transliterate(event["title"])
    puts "Add Film: \"#{title}\""
    url = "https://api.themoviedb.org/3/search/movie?api_key=#{ENV["IMDB_KEY"]}&language=fr-FR&query=#{title}&page=1"
    search_serialized = open(url).read
    search = JSON.parse(search_serialized)

    id = search["results"].first["id"]
    url = "https://api.themoviedb.org/3/movie/#{id}?api_key=#{ENV["IMDB_KEY"]}&language=fr-FR"
    movie_serialized = open(url).read
    movie = JSON.parse(movie_serialized)
    movie_save = Movie.create(tmdb_id: movie["id"], title: movie["title"], overview: movie["overview"], adult: movie["adult"], poster_path: movie["poster_path"], backdrop_path: movie["backdrop_path"], runtime: movie["runtime"])
    url = "https://api.themoviedb.org/3/movie/#{movie["id"]}/credits?api_key=#{ENV["IMDB_KEY"]}"
    credit_serialized = open(url).read
    credit = JSON.parse(credit_serialized)
    credit["cast"].first(5).each do |cast|
      credit = Credit.create(tmdb_id: cast["id"], character: cast["character"], name: cast["name"], profile_path: cast["profile_path"])
      movie_credit = MovieCredit.create(movie_id: movie_save.id, credit_id: credit.id, order: cast["order"])
    end
    event["sessions"].each do |session|
      show = Show.new(movie_id: movie_save.id, date: Time.parse(session["date"]))
      if session["version"] == "VF"
        show.language = "VF"
      else
        show.language = "VOSTFR"
      end
      show.save
    end
  end
end
