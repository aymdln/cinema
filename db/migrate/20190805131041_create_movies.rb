class CreateMovies < ActiveRecord::Migration[5.2]
  def change
    create_table :movies do |t|
      t.integer :tmdb_id
      t.string :title
      t.text :overview
      t.boolean :adult
      t.string :poster_path
      t.string :backdrop_path
      t.integer :runtime
      t.boolean :recommandation, default: false

      t.timestamps
    end
  end
end
