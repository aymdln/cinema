class CreateShows < ActiveRecord::Migration[5.2]
  def change
    create_table :shows do |t|
      t.references :movie, foreign_key: true
      t.datetime :date
      t.integer :language
      t.boolean :pourtous, default: false

      t.timestamps
    end
  end
end
