class CreateMovieCredits < ActiveRecord::Migration[5.2]
  def change
    create_table :movie_credits do |t|
      t.references :movie, foreign_key: true
      t.references :credit, foreign_key: true
      t.integer :order
    end
  end
end
