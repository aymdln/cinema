class CreateCredits < ActiveRecord::Migration[5.2]
  def change
    create_table :credits do |t|
      t.integer :tmdb_id
      t.string :character
      t.string :name
      t.string :profile_path

      t.timestamps
    end
  end
end
